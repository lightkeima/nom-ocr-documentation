## Nom OCR

Repository chưa document của đề tài Nôm OCR, các hình ảnh data sample được nằm trong repo này. 

Các document nằm ở mục Wiki của repo nà, có thể truy cập dựa vào các link bên dưới 

## Hướng dẫn

Khi các bạn update document vui lòng update luôn vào danh sách vào file readme này.

```
- [Tựa đề](đường dẫn)
```

<b>Đặt message của các commit:</b>

- Thêm file mới \[add\] 
- Cập nhật nội dung 1 file \[update\]
- Xóa một file \[remove\]

Nên tại branch mới và chỉnh sửa trên đó trước khi merge vào branch master.

## Các document trong repository này
- [Data Team Working Note](https://gitlab.com/lightkeima/nom-ocr-documentation/-/wikis/Data-team-working-note) 
- [Khái niệm, tiếp cận và một số thứ liên quan](https://gitlab.com/lightkeima/nom-ocr-documentation/-/wikis/B%C3%A0i-to%C3%A1n,-c%C3%A1c-h%C6%B0%E1%BB%9Bng-gi%E1%BA%A3i-hi%E1%BB%87n-t%E1%BA%A1i-kh%E1%BA%A3-thi)
## Links
- [Official data folder](https://drive.google.com/drive/folders/177B3tX7XpqWoHJ6cs28MESpO-H4L50No?usp=sharing)
- [Demo 1 Detect và Recognize](https://gitlab.com/lightkeima/nom-detection-demo)
- [Tool tự tạo bounding box cho chữ Nôm trên data ảnh giấy](https://gitlab.com/lightkeima/nom-segmentation-tool)
- [Working folder - data chưa hoàn thành, các file tạm thời lưu ở đây](https://drive.google.com/drive/folders/1nv8rHnhO1R0NbBeMHnTOB1cht9-lYgRT?usp=sharing)
##  Notebooks
- [Melnyknetwork - GAP](https://colab.research.google.com/drive/1NcbNu86KX9XsQbLeswz35Kj3jehyPRCw?usp=sharing)
- [MelnykNetwork- Siamese Network](https://colab.research.google.com/drive/1oQhwkqiQJUuQ9u10Tfae76-tllpsrvBk?usp=sharing)
